<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('firstname');
            $table->string('midname');
            $table->string('lastname');
            $table->string('suffix')->nullable();
            $table->string('email')->unique();
            $table->string('address');
            $table->string('zipcode');
            $table->string('phone');
            $table->string('citizenship');
            $table->string('userType')->default('Professional');
            $table->string('rdo');
            $table->string('atc');
            $table->string('tin');
            $table->string('lob');
            $table->string('individual')->default('person');
            $table->date('bdate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user_info');
    }
}
