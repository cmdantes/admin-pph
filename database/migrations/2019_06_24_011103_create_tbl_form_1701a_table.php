<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblForm1701aTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_form_1701a', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
//            $table->unsignedBigInteger('question_id');
            $table->string('billingNo');
            $table->string('quarter');
            $table->string('year');
            $table->string('amended');
            $table->string('returnPeriod');
            $table->string('salesRevenue');
            $table->string('salesReturn');
            $table->string('netSales');
            $table->string('nonOperatingDesc1');
            $table->string('nonOperatingAmount1');
            $table->string('nonOperatingDesc2');
            $table->string('nonOperatingAmount2');
            $table->string('totalNonOperatingIncome');
            $table->string('totalTaxableIncome');
            $table->string('allowableReductionFromGross');
            $table->string('taxableIncomeToDate');
            $table->string('taxDue');
            $table->string('page2PriorYear');
            $table->string('page2TaxPayment');
            $table->string('page2Tax3Quarter');
            $table->string('page2Creditable');
            $table->string('page2TaxPaidReturn');
            $table->string('page2OtherTaxCredits');
            $table->string('page2TotalTaxCredits');
            $table->string('page2TotalNetTaxable');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_form_1701a');
    }
}
