<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNonOperatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_non_operating', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('billingNo');
            $table->string('description0');
            $table->string('amount0');
            $table->string('description1');
            $table->string('amount1');
            $table->string('description2');
            $table->string('amount2');
            $table->string('description3');
            $table->string('amount3');
            $table->string('description4');
            $table->string('amount4');
            $table->string('description5');
            $table->string('amount5');
            $table->string('description6');
            $table->string('amount6');
            $table->string('description7');
            $table->string('amount7');
            $table->string('description8');
            $table->string('amount8');
            $table->string('description9');
            $table->string('amount9');
            $table->string('description10');
            $table->string('amount10');
            $table->string('description11');
            $table->string('amount11');
            $table->string('description12');
            $table->string('amount12');
            $table->string('description13');
            $table->string('amount13');
            $table->string('description14');
            $table->string('amount14');
            $table->string('description15');
            $table->string('amount15');
            $table->string('description16');
            $table->string('amount16');
            $table->string('description17');
            $table->string('amount17');
            $table->string('description18');
            $table->string('amount18');
            $table->string('description19');
            $table->string('amount19');
            $table->string('description20');
            $table->string('amount20');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_non_operating');
    }
}
