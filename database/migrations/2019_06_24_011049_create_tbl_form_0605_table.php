<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblForm0605Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_form_0605', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('question_id');
            $table->string('period');
            $table->date('yearEnded');
            $table->string('quarter');
            $table->date('dueDate');
            $table->date('returnPeriod');
            $table->string('atc');
            $table->string('taxType');
            $table->string('classification')->default("Individual");
            $table->string('mannerPayment');
            $table->string('others')->nullable();
            $table->string('typePayment');
            $table->integer('installmentNo');
            $table->string('voluntaryPayment')->nullable();
            $table->string('perAudit')->nullable();
            $table->string('investigatingOfficer')->nullable();
            $table->float('taxDeposit');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_form_0605');
    }
}
