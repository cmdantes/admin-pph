<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_billing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('billingNo');
            $table->string('quarter');
            $table->string('year');
            $table->string('amended');
            $table->string('returnPeriod');
            $table->string('salesRevenueForThisQuarter');
            $table->string('nonOperatingIncome');
            $table->string('nonOperatingIncomeDesc');
            $table->string('totalIncomeOfTheQuarter');
            $table->string('cumulativeTaxableIncome');
            $table->string('allowableReductionFromGross');
            $table->string('taxableIncomeToDate');
            $table->string('taxDue');
            $table->string('priorYearExcessCredits');
            $table->string('taxPaymentForThePreviousQuarter');
            $table->string('creditableTaxWithheldForThePreviousQuarter');
            $table->string('creditableTaxWithheldPerBIRFormNo2307');
            $table->string('taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn');
            $table->string('status')->default("Yes");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_billing');
    }
}
