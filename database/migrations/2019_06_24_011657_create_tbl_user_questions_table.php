<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUserQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            // $table->foreign('user_id')->references('id')->on('tbl_users');
            $table->longText('questions');
            $table->longText('answer')->nullable('Not Answered Yet');
            //  $table->primary(array('id','user_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_user_questions');
    }
}
