<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    protected $table = 'tbl_dashboard';

    protected $fillable = ['user_id', 'year', 'first', 'second', 'third'];

    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }
}
