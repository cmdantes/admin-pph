<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Dashboard;
use App\Form0605;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillingsController extends Controller
{

    public function index(){
        return view('users.billings');
    }

    function createBillings(Request $data){
         Billing::create([
            'user_id' => Auth::user()->id,
            'year' => $data['year'],
            'quarter' => $data['quarter'],
            'amended' => $data['amended'],
            'returnPeriod' => $data['returnPeriod'],
            'billingNo' => $data['billingNo'],
            'salesRevenueForThisQuarter' => $data['salesRevenueForThisQuarter'],
            'nonOperatingIncome' => $data['nonOperatingIncome'],
            'nonOperatingIncomeDesc' => $data['nonOperatingIncomeDesc'],
            'totalIncomeOfTheQuarter' => $data['totalIncomeOfTheQuarter'],
            'cumulativeTaxableIncome' => $data['cumulativeTaxableIncome'],
            'allowableReductionFromGross' => $data['allowableReductionFromGross'],
            'taxableIncomeToDate' => $data['taxableIncomeToDate'],
            'taxDue' => $data['taxDue'],
            'priorYearExcessCredits' => $data['priorYearExcessCredits'],
            'taxPaymentForThePreviousQuarter' => $data['taxPaymentForThePreviousQuarter'],
            'creditableTaxWithheldForThePreviousQuarter' => $data['creditableTaxWithheldForThePreviousQuarter'],
            'creditableTaxWithheldPerBIRFormNo2307' => $data['creditableTaxWithheldPerBIRFormNo2307'],
            'taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn' => $data['taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn'],
        ]);
        $user = Billing::all()->where('user_id', $data->user_id)->first();
        return response()->json($user);
    }

    function getBillings(Request $request)
    {
        $client = Billing::where('user_id', $request->user_id)->get();
        return response()->json($client);
    }

    function editBillings(Request $request)
    {
        $bill = Billing::find($request->update_id);
        $bill->update([
            'user_id' =>  $request->input('user_id', $bill->user_id),
            'year' =>  $request->input('year', $bill->year),
            'quarter' => $request->input('quarter', $bill->quarter),
            'amended' => $request->input('amended', $bill->amended),
            'returnPeriod' => $request->input('returnPeriod', $bill->returnPeriod),
            'billingNo' => $request->input('billingNo', $bill->billingNo),
            'salesRevenueForThisQuarter' => $request->input('salesRevenueForThisQuarter', $bill->salesRevenueForThisQuarter),
            'nonOperatingIncome' => $request->input('nonOperatingIncome', $bill->nonOperatingIncome),
            'nonOperatingIncomeDesc' => $request->input('nonOperatingIncomeDesc', $bill->nonOperatingIncomeDesc),
            'totalIncomeOfTheQuarter' => $request->input('totalIncomeOfTheQuarter', $bill->totalIncomeOfTheQuarter),
            'cumulativeTaxableIncome' => $request->input('cumulativeTaxableIncome', $bill->cumulativeTaxableIncome),
            'allowableReductionFromGross' => $request->input('allowableReductionFromGross', $bill->allowableReductionFromGross),
            'taxableIncomeToDate' => $request->input('taxableIncomeToDate', $bill->taxableIncomeToDate),
            'taxDue' => $request->input('taxDue', $bill->taxDue),
            'priorYearExcessCredits' => $request->input('priorYearExcessCredits', $bill->priorYearExcessCredits),
            'taxPaymentForThePreviousQuarter' => $request->input('taxPaymentForThePreviousQuarter', $bill->taxPaymentForThePreviousQuarter),
            'creditableTaxWithheldForThePreviousQuarter' => $request->input('creditableTaxWithheldForThePreviousQuarter', $bill->creditableTaxWithheldForThePreviousQuarter),
            'creditableTaxWithheldPerBIRFormNo2307' => $request->input('creditableTaxWithheldPerBIRFormNo2307', $bill->creditableTaxWithheldPerBIRFormNo2307),
            'taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn' => $request->input('taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn', $bill->taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn),
        ]);

        $bill->save();
        return $bill;
    }

    function actionlist(){
        $bill = Billing::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $bill->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
        }
        $bills = $bill->where('user_id', Auth::user()->id)->get();
        return datatables()->of($bills)->make(true);
    }
    public function showCreateBillings(Billing $b){
        return view('users.createBilling',compact(['b']));
    }

    public function deleteBillings(Request $request){
        $f06 = Billing::where('id', $request->id)->
        where('user_id', $request->user_id)->first()->delete();
        if(isset($f06)) {
            return 'Success';
        }
    }

    public function fileBillings(Request $request){
        $f06 = Billing::where('id', $request->id)->
        where('user_id', $request->user_id)->first();
        if(isset($f06)) {
            return 'Success';
        }
    }

    public function getTaxDue(Request $request){
        $bill = Dashboard::where('user_id', $request->user_id)->where('year', $request->year)->get(['first','second','third']);
        return response()->json($bill);
    }

    public function getBillingNo(Request $request)
    {
        $bill = Form0605::where('user_id', $request->user_id)->get('id');
        return (string) $bill;
    }
}
