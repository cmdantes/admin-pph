<?php

namespace App\Http\Controllers;

use App\Form1701a;
use App\NonOperating;
use App\UserFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Form1701aController extends Controller
{
    public function index(Request $request){
//        print_r($form1);
        return view('users.form1701a');
    }

    public function showCreateForm(Form1701a $form1701a){

        return view('users.createform1701a',compact(['form1701a']));
    }

    public function actionlist(){

            $client = Form1701a::query();

            $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
            $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

            if($start_date && $end_date){

                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));
                $client->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
            }
            $clients = $client->where('user_id', Auth::user()->id)->get();
            return datatables()->of($clients)->make(true);


    }

    public function create(Request $data){
         Form1701a::create([
            'user_id' => Auth::user()->id,
            'quarter' => $data['quarter'],
            'year' => $data['year'],
             'billingNo' => $data['billingNo'],
             'amended' => $data['amended'],
            'returnPeriod' => $data['returnPeriod'],
            'salesRevenue' => $data['salesRevenue'],
            'salesReturn' => $data['salesReturn'],
            'netSales' => $data['netSales'],
            'nonOperatingDesc1' => $data['nonOperatingDesc1'],
            'nonOperatingAmount1' => $data['nonOperatingAmount1'],
            'nonOperatingDesc2' => $data['nonOperatingDesc2'],
            'nonOperatingAmount2' => $data['nonOperatingAmount2'],
            'totalNonOperatingIncome' => $data['totalNonOperatingIncome'],
            'totalTaxableIncome' => $data['totalTaxableIncome'],
            'allowableReductionFromGross' => $data['allowableReductionFromGross'],
            'taxableIncomeToDate' => $data['taxableIncomeToDate'],
            'taxDue' => $data['taxDue'],
            'page2PriorYear' => $data['page2PriorYear'],
            'page2TaxPayment' => $data['page2TaxPayment'],
            'page2Tax3Quarter' => $data['page2Tax3Quarter'],
            'page2Creditable' => $data['page2Creditable'],
            'page2TaxPaidReturn' => $data['page2TaxPaidReturn'],
            'page2OtherTaxCredits' => $data['page2OtherTaxCredits'],
            'page2TotalTaxCredit' => $data['page2TotalTaxCredit'],
            'page2TotalNetTaxable' => $data['page2TotalNetTaxable'],
            'status' => $data['status'],
        ]);
        $form1701a = Form1701a::all()->where('user_id', $data->user_id)->first();
        return response()->json($form1701a);
    }

    public function updateForm1701a(Request $request){

        $f1701a = Form1701a::find($request->update_id);
        $f1701a->update([
            'user_id' =>  $request->input('user_id', $f1701a->user_id),
            'quarter' => $request->input('quarter', $f1701a->quarter),
            'year' => $request->input('year', $f1701a->year),
            'amended' => $request->input('amended', $f1701a->amended),
            'returnPeriod' => $request->input('returnPeriod', $f1701a->returnPeriod),
            'salesRevenue' => $request->input('salesRevenue', $f1701a->salesRevenue),
            'salesReturn' => $request->input('salesReturn', $f1701a->salesReturn),
            'netSales' => $request->input('netSales', $f1701a->netSales),
            'nonOperatingDesc1' => $request->input('nonOperatingDesc1', $f1701a->nonOperatingDesc1),
            'nonOperatingAmount1' => $request->input('nonOperatingAmount1', $f1701a->nonOperatingAmount1),
            'nonOperatingDesc2' => $request->input('nonOperatingDesc2', $f1701a->nonOperatingDesc2),
            'nonOperatingAmount2' => $request->input('nonOperatingAmount2', $f1701a->nonOperatingAmount2),
            'totalNonOperatingIncome' => $request->input('totalNonOperatingIncome', $f1701a->totalNonOperatingIncome),
            'totalTaxableIncome' => $request->input('totalTaxableIncome', $f1701a->totalTaxableIncome),
            'allowableReductionFromGross' => $request->input('allowableReductionFromGross', $f1701a->allowableReductionFromGross),
            'taxableIncomeToDate' => $request->input('taxableIncomeToDate', $f1701a->taxableIncomeToDate),
            'taxDue' => $request->input('taxDue', $f1701a->taxDue),
            'page2PriorYear' => $request->input('page2PriorYear', $f1701a->page2PriorYear),
            'page2TaxPayment' => $request->input('page2TaxPayment', $f1701a->page2TaxPayment),
            'page2Tax3Quarter' => $request->input('page2Tax3Quarter', $f1701a->page2Tax3Quarter),
            'page2Creditable' => $request->input('page2Creditable', $f1701a->page2Creditable),
            'page2TaxPaidReturn' => $request->input('page2TaxPaidReturn', $f1701a->page2TaxPaidReturn),
            'page2OtherTaxCredits' => $request->input('page2OtherTaxCredits', $f1701a->page2OtherTaxCredits),
            'page2TotalTaxCredit' => $request->input('page2TotalTaxCredit', $f1701a->page2TotalTaxCredit),
            'page2TotalNetTaxable' => $request->input('page2TotalNetTaxable', $f1701a->page2TotalNetTaxable),
            'status' => $request->input('status', $f1701a->status),
        ]);
        $f1701a->save();
        return $f1701a;
    }

    public function getData(Request $request){
        $form17 = Form1701a::where('user_id', $request->user_id)->get();
        return response()->json($form17);
    }

    public function saveNonOperating(Request $data){
        return NonOperating::create([
            'user_id' => Auth::user()->id,
            'billingNo' => $data['billingNo'],
            'description0' => $data['description0'],
            'description1' => $data['description1'],
            'description2' => $data['description2'],
            'description3' => $data['description3'],
            'description4' => $data['description4'],
            'description5' => $data['description5'],
            'description6' => $data['description6'],
            'description7' => $data['description7'],
            'description8' => $data['description8'],
            'description9' => $data['description9'],
            'description10' => $data['description10'],
            'description11' => $data['description11'],
            'description12' => $data['description12'],
            'description13' => $data['description13'],
            'description15' => $data['description15'],
            'description16' => $data['description16'],
            'description17' => $data['description17'],
            'description18' => $data['description18'],
            'description19' => $data['description19'],
            'description20' => $data['description20'],
            'description14' => $data['description14'],
            'amount0' => $data['amount0'],
            'amount1' => $data['amount1'],
            'amount2' => $data['amount2'],
            'amount3' => $data['amount3'],
            'amount4' => $data['amount4'],
            'amount5' => $data['amount5'],
            'amount6' => $data['amount6'],
            'amount7' => $data['amount7'],
            'amount8' => $data['amount8'],
            'amount9' => $data['amount9'],
            'amount10' => $data['amount10'],
            'amount11' => $data['amount11'],
            'amount12' => $data['amount12'],
            'amount13' => $data['amount13'],
            'amount14' => $data['amount14'],
            'amount15' => $data['amount15'],
            'amount16' => $data['amount16'],
            'amount17' => $data['amount17'],
            'amount18' => $data['amount18'],
            'amount19' => $data['amount19'],
            'amount20' => $data['amount20'],
        ]);
//        $form1701a = NonOperating::all()->where('user_id', $data->user_id)->first();
//        return response()->json($form1701a);
    }

    public function getNon(Request $request){
        return NonOperating::where('user_id', $request->user_id)
            ->where('billingNo', $request->billingNo)
            ->where('description0', $request->description0)
            ->where('description1', $request->description1)
            ->where('description2', $request->description2)
            ->where('description3', $request->description3)
            ->where('description4', $request->description4)
            ->where('description5', $request->description5)
            ->where('description6', $request->description6)
            ->where('description7', $request->description7)
            ->where('description8', $request->description8)
            ->where('description9', $request->description9)
            ->where('description10', $request->description10)
            ->where('description11', $request->description11)
            ->where('description12', $request->description12)
            ->where('description13', $request->description13)
            ->where('description14', $request->description14)
            ->where('description15', $request->description15)
            ->where('description16', $request->description16)
            ->where('description17', $request->description17)
            ->where('description18', $request->description18)
            ->where('description19', $request->description19)
            ->where('description20', $request->description20)
            ->where('amount0', $request->amount0)
            ->where('amount1', $request->amount1)
            ->where('amount2', $request->amount2)
            ->where('amount3', $request->amount3)
            ->where('amount4', $request->amount4)
            ->where('amount5', $request->amount5)
            ->where('amount6', $request->amount6)
            ->where('amount7', $request->amount7)
            ->where('amount8', $request->amount8)
            ->where('amount9', $request->amount9)
            ->where('amount10', $request->amount10)
            ->where('amount11', $request->amount11)
            ->where('amount12', $request->amount12)
            ->where('amount13', $request->amount13)
            ->where('amount14', $request->amount14)
            ->where('amount15', $request->amount15)
            ->where('amount16', $request->amount16)
            ->where('amount17', $request->amount17)
            ->where('amount18', $request->amount18)
            ->where('amount19', $request->amount19)
            ->where('amount20', $request->amount20)
            ->get(
            ['description0','description1','description2','description3','description4','description5','description6','description7','description8','description9','description10','description11','description12','description13','description14','description15','description16','description17','description18','description19','description20',
                'amount0','amount2','amount1','amount3','amount4','amount5','amount6','amount7','amount8','amount9','amount10','amount11','amount12','amount13','amount14','amount15','amount16','amount17','amount18','amount19','amount20']
        );
    }
    public function deleteForm1701a(Request $request){
        $f06 = Form1701a::where('id', $request->id)->
        where('user_id', $request->user_id)->first()->delete();
        if(isset($f06)) {
            return 'Success';
        }
    }

    public function fileform1701a(Request $request){
        $f06 = Form1701a::where('id', $request->id)->
        where('user_id', $request->user_id)->first();
        if(isset($f06)) {
            return 'Success';
        }
    }

    public function createReport(Request $request)
    {
        $f06 = UserFile::where('id', $request->id)->
        where('user_id', $request->user_id)->first();
        return response()->json($f06);
//        if ($request->hasFile('file')) {
//            $filenameWithExt = $request->file->getClientOriginalName();
//            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
//            $extension = $request->file->getClientOriginalExtension();
//            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
//            $path = $request->file('file')->storeAs('public', $fileNameToStore);
//        }
//        $form = new UserFile();
//        $form->user_id = auth()->user()->id;
//        $form->fileName = $fileNameToStore;
//        $form->fileLink = $path;
//        $form->save();
//        return 'Success';
    }
}
