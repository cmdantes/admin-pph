<?php

namespace App\Http\Controllers;

use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServicesController extends Controller
{
    public function index(){
        return view('users.services');
    }

    public function getServices(Request $request)
    {
        $client = Services::where('user_id', $request->user_id)->get();
        return response()->json($client);
    }

    public function create(Request $data){
        Services::create([
            'user_id' => Auth::user()->id,
            'serviceNo' => $data['serviceNo'],
            'serviceName' => $data['serviceName'],
            'amount' => $data['amount'],
        ]);
        $serv = Services::all()->where('user_id', $data->user_id)->first();
        return response()->json($serv);
    }

    public function getServiceNo(Request $data)
    {
        $client = Services::where('user_id', $data->user_id)->first();

        if(isset($client)) {
            return array("User Email");
        }
    }

    public function deleteServices(Request $request){
        $serv = Services::where('user_id', $request->user_id)->where('serviceNo', $request->serviceNo)->delete();
        if(isset($serv)) {
            return 'Success';
        }

    }

    public function destroy( Request $request)
    {
        $client = Services::where('id', $request->category_id)->delete();
        return 'Success';
    }

    public function updateServices(Request $request)
    {
        $serv = Services::find($request->update_id);
        $serv->update([
            'user_id' =>  $request->input('user_id', $serv->user_id),
            'serviceNo' => $request->input('serviceNo', $serv->serviceNo),
            'serviceName' => $request->input('serviceName', $serv->serviceName),
            'amount' => $request->input('amount', $serv->amount),
        ]);
        $serv->save();
        return $serv;
    }

    public function showCreateForm(Services $serv){
        return view('users.createServices',compact(['serv']));
    }

    function actionlist()
        {
            $client = Services::query();

            $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
            $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

            if($start_date && $end_date){

                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));
                $client->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
            }
            $clients = $client->where('user_id', Auth::user()->id)->get();
            return datatables()->of($clients)->make(true);
        }


}
