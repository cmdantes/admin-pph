<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Client;
use App\FAQS;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $clients = Client::where('user_id', Auth::user()->id)->count();
        $service = Services::where('user_id', Auth::user()->id)->count();
        $billings = Billing::where('user_id', Auth::user()->id)->count();
        $faqs = FAQS::where('user_id', Auth::user()->id)->count();

        return view('users.home', compact(array('clients','service','billings','faqs')));
    }



}
