<?php

namespace App\Http\Controllers\Auth;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        //defining our middleware for this controller
        $this->middleware('guest:admin',['except' => ['logout']]);
    }

    //function to show admin login form
    public function showLoginForm() {
        return view('auth.admin-login');
    }
    //function to login admins
    public function login(Request $request) {
        if ($request->ajax()) {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required'
            ]);
            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
//                echo 'success';
            return response()->json('success');
            }
            return redirect()->back()->withInput($request->only('email', 'remember'));
        }
    }

    public function logout(Request $request)
    {
        $this->middleware('prevent-back-history');
        Auth::logout();
        Session::flush();
        Redirect::back();
        return redirect('/admin-pph');
    }
}
