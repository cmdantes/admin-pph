<?php

namespace App\Http\Controllers\Auth;

use App\Admin;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AdminRegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/admin/login';

    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showRegisterForm() {
        return view('auth.admin-register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'name' =>['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $data)
    {
        return Admin::create([

            'username' => $data['username'],
            'name' =>$data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    protected function redirectTo()
    {
        return '/admin/login';
    }
   /* public function store()
    {
        $this->validate(request(), [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = Admin::create(request(['username', 'email', 'password']));

        auth()->login($user);

        return redirect()->to('auth.admin-login');
    }*/
}
