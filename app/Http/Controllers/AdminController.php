<?php

namespace App\Http\Controllers;

use App\Billing;
use App\FAQS;
use App\Services;
use App\User;
use App\Questions;
use App\UserInfo;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    // VIEW
    public function index(Agent $agent, Request $q)
    {
        $users = User::paginate(10);
        $userinf = User::where("id", $q->id)->first();
        // dd($tickets);
        return view('admins.indexes', compact(['users','agent','userinf']));
//        else if($agent->is())
    }

    public function indexs(User $user)
    {

        $users = User::paginate(10);
        // dd($tickets);
        return view('admins.indexes', compact(['users', 'admin']));
    }

    public function dashes()
    {
        $users = User::count();
        $bill = Billing::count();
        $serv = Services::count();
        $client = \App\Client::count();
//        dd($users);
//        $q = Questions::count();
        return view('admins.dash', compact(['users','bill','serv','client']));
    }

    public function questions(Questions $question, Agent $agent)
    {
        return view('admins.questions', compact(['question','agent']));

    }
    public function show(User $user)
    {
        // $statuses = Status::get();
        return view('admins.show', compact(['user']));
    }

    function updateStatus(Request $request){
        $db = User::where("id",$request->id)->first();
        return response()->json($db);
    }
    //  UPDATE


    function statUpdate(Request $request){
        $user = User::find($request->update_id);
        $user->update([
            'name' => $request->input('name', $user->name),
            'email' => $request->input('email', $user->email),
            'password' => $request->input('password', $user->password),
        ]);
        $user->status = request('status');

        $user->save();

        return response()->json("success");
    }

    public function update(Request $request)
    {
        $user = User::find($request->update_id);
//        $user->name = $request->user;
//        $user->email = $request->email;
//        $user->password = $request->user;
        $user->update([
            'name' => $request->input('name', $user->name),
            'email' => $request->input('email', $user->email),
            'password' => $request->input('password', $user->password),
        ]);
        $user->status = request('status');

        $user->save();

//        return back();
        return redirect()->route('admins.index', compact(['user']));
//        return $user;
    }





    function actionss() {
            $user = User::query();
                //            $user = DB::table('tbl_users')->join('tbl_user_info','tbl_user_info.user_id', "=",'tbl_users.id');

            $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
                $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

            if ($start_date && $end_date) {

                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));
                $user->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");

//                ->whereRaw("tbl_users.created_at >= '" . $start_date . "' AND tbl_users.created_at <= '" . $end_date . "'");
            }
//        $users = $user->where('status','=','active');
        $users = $user->select('*');
        return datatables()->of($users)->make(true);
    }

    function anot(Request $request){

        $db = UserInfo::where("user_id",$request->id)->first();
        return $db;

    }

    public function load(Request $q)
    {
        $user = User::where('id', $q->id)->get();
        return view('admins.indexes', compact('user'));
    }

    public function destroy(User $user, Request $request)
    {
        $user = User::where('id', $request->id)->delete();
        return response()->json('success');
//        return redirect()->route('admins.index')->withSuccess('User has been Deleted');
    }

    public function delete(User $user)
    {
        return view('admins.delete', compact('user'));
    }

    // Unused Functions
    // Unused Functions
    // Unused Functions
    // Unused Functions
    // Unused Functions
    // Unused Functions
    // Unused Functions
    // Unused Functions

    /*function  action2(Request $request){
        if($request->ajax()){
            $output = '';
            if($request->start_date != '' && $request->end_date != '') {
                $s = 2;
                $users = DB::table('tbl_users')
                    ->whereBetween('created_at', [$request->get('start_date'), $request->get('end_date')])->get();
            }
            elseif($request->start_date != '' && $request->end_date != '') {
                $s = 2;
                $users = DB::table('tbl_users')
                    ->whereBetween('created_at', [$request->get('start_date'), $request->get('end_date')])->get();
            }
            else{
                $users = DB::table('tbl_users')->orderBy('created_at', 'desc')->get();
            }

            $total_row = $users->count();
            if($total_row > 0)
            {
                foreach($users as $user)
                {
                    $output .= '
                    <tr>
                     <td>'.$user->name.'</td>
                     <td>'.$user->username.'</td>
                     <td>'.$user->email.'</td>
                     <td>'.$user->phone.'</td>
                     <td>'.$user->created_at.'</td>
                     <td><a href="/admin/update/'.$user->id.'" class="btn btn-secondary">Update Status</a></td>
                     <td><a href="/admin/delete/'.$user->id.'" class="btn btn-danger">Delete</a></td></td>
                    </tr>
                    ';
                }
            }
            else
            {
                $output = '<tr><td align="center" colspan="5">No Data Found</td></tr>';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row,
                'sample'  => $s
            );
            echo json_encode($data);
        }
    }

    function action(Request $request, User $users)
    {
        if($request->ajax())
        {
            $output = '';
            $query = $request->get('query');
            if($request->date_search != '' || $query != '')
            {
                $s = '1 - '. $query;
                $users = DB::table('tbl_users')
                    ->where('name', 'like', '%'.$query.'%')
                    ->orWhere('username', 'like', '%'.$query.'%')
                    ->orWhere('email', 'like', '%'.$query.'%')
                    ->orWhere('phone', 'like', '%'.$query.'%')
                    ->orWhere('created_at', 'like', '%'.$query.'%')
                    ->orderBy('id', 'desc')
                    ->get();
            }
            elseif($request->start_date != '' && $request->end_date != '') {
                $s = 2;
                $users = DB::table('tbl_users')
                    ->whereBetween('created_at', [$request->get('start_date'), $request->get('end_date')])->get();
            }
            elseif($request->searchdate == 'yes') {
                $s = 2;
                $users = DB::table('tbl_users')
                    ->whereBetween('created_at', [$request->get('start_date'), $request->get('end_date')])->get();
            }
            else
            {
                $s = '3 - '.$request->start_date.' - '.$request->end_date;
                $users = DB::table('tbl_users')->orderBy('created_at', 'desc')->get();
            }
            $total_row = $users->count();
            if($total_row > 0)
            {
                foreach($users as $user)
                {
                    $output .= '
                    <tr>
                     <td>'.$user->name.'</td>
                     <td>'.$user->username.'</td>
                     <td>'.$user->year.'</td>
                     <td>'.$user->phone.'</td>
                     <td>'.$user->created_at.'</td>
                     <td><a href="/admin/update/'.$user->id.'" class="btn btn-secondary">Update Status</a></td>
                     <td><a href="/admin/delete/'.$user->id.'" class="btn btn-danger">Delete</a></td></td>
                    </tr>
                    ';
                }
            }
            else
            {
                $output = '
               <tr>
                <td align="center" colspan="5">No Data Found</td>
               </tr>
               ';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row,
                'sample'  => $s
            );

            echo json_encode($data);
        }
    }
    public function questionsshow(Questions $question)
    {
        return view('admins.showanswer', compact(['question']));
    }
}*/
}
