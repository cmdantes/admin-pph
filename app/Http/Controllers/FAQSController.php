<?php

namespace App\Http\Controllers;

use App\FAQS;
use App\Form1701a;
use App\Likes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FAQSController extends Controller
{
    public function store(Request $request)
    {
        $questions = FAQS::create([
            'user_id' => Auth::user()->id,
            'questions' => $request->questions,
            'answers' => $request->answers,
        ]);
//        return redirect('/user');
        return 'Success';
        /* $question = new Questions();
         $question->questions = request('questions');
         $question->save();*/
    }

    public function getAnswers(Request $request){
        return FAQS::where('id',$request->id)->get(['questions_no','answers','questions']);

//        return Form1701a::where('question_id', $request->question_id)->get(['questions_no','answers','questions']);
    }

    public function getLikeQuestion(Request $request){
        return Likes::where('question_id',$request->question_id)->get('likes');
    }

    public function likeQuestion(Request $request){
        $a = Likes::where('question_id',$request->question_id)->where('user_id',$request->user_id)->get('likes');
        return (string) $a;

    }

    public  function getPopularQuestions(){
        return FAQS::all();

    }
}
