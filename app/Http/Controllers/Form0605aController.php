<?php

namespace App\Http\Controllers;

use App\Form0605;
use App\UserFile;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Form0605aController extends Controller
{
    public function index()
    {
        return view('users.form0605');
    }

    public function actionlist()
    {
        $form0605 = Form0605::query();

            $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
            $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

            if ($start_date && $end_date) {
                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));
                $form0605->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
            }
            $form0605s = $form0605->where('user_id', Auth::user()->id)->get();
            return datatables()->of($form0605s)->make(true);
    }

    public function showCreate(Form0605 $form0605)
    {
        return view('users.createform0605', compact(['form0605']));
    }

    public function create(Request $data)
    {
        Form0605::create([
            'user_id' => Auth::user()->id,
            'period' => $data['period'],
            'yearEnded' => $data['yearEnded'],
            'quarter' => $data['quarter'],
            'dueDate' => $data['dueDate'],
            'returnPeriod' => $data['returnPeriod'],
            'atc' => $data['atc'],
            'taxType' => $data['taxType'],
            'classification' => $data['classification'],
            'mannerPayment' => $data['mannerPayment'],
            'others' => $data['others'],
            'typePayment' => $data['typePayment'],
            'installmentNo' => $data['installmentNo'],
            'voluntaryPayment' => $data['voluntaryPayment'],
            'perAudit' => $data['perAudit'],
            'investigatingOfficer' => $data['investigatingOfficer'],
            'taxDeposit' => $data['taxDeposit'],
            'status' => $data['status'],
        ]);

        $form0605 = Form0605::all()->where('user_id', $data->user_id)->first();
        return response()->json($form0605);
//        return redirect()->route('clients.index')->withSuccess('User has been Deleted');
    }

    public function getData(Request $request)
    {
//        $form0605 = Form0605::find($id);
//        return DB::table('tbl_form_0605')->where('user_id')->pluck('user_id')->toArray();
//        return view('users.form0605', compact(['form0605']));
        //GEt
//        return Form0605::get(['user_id', 'id', 'quarter'])->toArray();
        //Endget
//        return Form0605::where('id', $id)->first();
        /*$form0605 = Form0605::get(['user_id', 'id', 'quarter'])->find($request->input('user_id'));
        return response()->json($form0605);*/
        $form0605 = Form0605::where('user_id', $request->user_id)->first();
        return response()->json($form0605);
    }

    public function update(Request $request)
    {
        $f0605 = Form0605::find($request->update_id);
        $f0605->update([
            'user_id' => $request->input('user_id', $f0605->user_id),
            'period' => $request->input('period', $f0605->period),
            'yearEnded' => $request->input('yearEnded', $f0605->yearEnded),
            'quarter' => $request->input('quarter', $f0605->quarter),
            'dueDate' => $request->input('dueDate', $f0605->dueDate),
            'returnPeriod' => $request->input('returnPeriod', $f0605->returnPeriod),
            'atc' => $request->input('atc', $f0605->atc),
            'taxType' => $request->input('taxType', $f0605->salesReturn),
            'classification' => $request->input('classification', $f0605->classification),
            'mannerPayment' => $request->input('mannerPayment', $f0605->mannerPayment),
            'others' => $request->input('others', $f0605->others),
            'typePayment' => $request->input('typePayment', $f0605->typePayment),
            'installmentNo' => $request->input('installmentNo', $f0605->installmentNo),
            'voluntaryPayment' => $request->input('voluntaryPayment', $f0605->voluntaryPayment),
            'perAudit' => $request->input('perAudit', $f0605->perAudit),
            'investigatingOfficer' => $request->input('investigatingOfficer', $f0605->investigatingOfficer),
            'taxDeposit' => $request->input('taxDeposit', $f0605->taxDeposit),
            'status' => $request->input('status', $f0605->status),
        ]);
        $f0605->save();
        return $f0605;
    }

    public function deleteForm0605(Request $request)
    {
        $f06 = Form0605::where('id', $request->id)->where('user_id', $request->user_id)->delete();
        if (isset($f06)) {
            return 'Success';
        }
    }

    public function fileform(Request $request)
    {
        $f06 = Form0605::where('id', $request->id)->
        where('user_id', $request->user_id)->first();
        if (isset($f06)) {
            return 'Success';
        }
    }

    public function createReport(Request $request)
    {
        $f06 = UserFile::where('id', $request->id)->
        where('user_id', $request->user_id)->first();
        return response()->json($f06);

//        if ($request->hasFile('file')) {
//            $filenameWithExt = $request->file->getClientOriginalName();
//            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
//            $extension = $request->file->getClientOriginalExtension();
//            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
//            $path = $request->file('file')->storeAs('public', $fileNameToStore);
//        }
//        $form = new UserFile();
//        $form->user_id = auth()->user()->id;
//        $form->fileName = $fileNameToStore;
//        $form->fileLink = $path;
//        $form->save();
//        return 'Success';
    }
}
