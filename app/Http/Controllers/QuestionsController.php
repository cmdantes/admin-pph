<?php

namespace App\Http\Controllers;

use App\FAQS;
use App\Questions;
use App\XML;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;

class QuestionsController extends Controller
{


    public function store(Request $request)
    {
        $questions = Questions::create([
            'user_id' => Auth::user()->id,
            'questions' => $request->questions,

        ]);
//        return redirect('/user');
        return 'Success';
        /* $question = new Questions();
         $question->questions = request('questions');
         $question->save();*/
    }

    public function questions(Agent $agent){
        $questions = DB::table('tbl_user_questions')->where('user_id', Auth::user()->id)->get();
        return view('users.questions', compact(['questions', 'agent']));
    }


    public function insertFaqs(Request $request){
        if ($request->id != ""){
            if(is_array($request->id)) {
                foreach ($request->id as $row) {
                    $num = FAQS::all()->last()->id;
                    $result = Questions::where('id', $row)->first();
                    if ($result) {
                        if ($result->answer == '') {
                            return response()->json('answer');
                        } else {
                            $question = new FAQS();
                            $question->questions_no = 'Q'.$num;
                            $question->questions = $num.'. '.$result->questions;
                            $question->answers = $result->answer;
                            $question->views = 0;
                            $question->likes = 0;
                            $question->created_at = $result->created_at;
                            $question->updated_at = $result->updated_at;
                            $data = $question->save();
                            if ($data) {
                                Questions::where('id', $row)->delete();
                            }
                        }
                    }
                }
                return response()->json("success");
            }
        }

    }

    public function createReport(Request $request)
    {
        if ($request->hasFile('file')) {
            $filenameWithExt = $request->file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file->getClientOriginalExtension();
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('file')->storeAs('public', $fileNameToStore);
        }
        $form = new XML();
        $form->user_id = 2;
        $form->form_id = 1;
        $form->form = 1;
        $form->file = $fileNameToStore;
        $form->file_name = $path;
        $form->returnPeriod = $path;
        $form->status = $path;
        $form->save();
        return 'Success';
    }
    function faqsupdate(Request $request){
        $question = FAQS::find($request->questionnaire_id);
        $question->update([
            'user_id' => $request->input('user_id', $question->user_id),
            'questions' => $request->input('questions', $question->questions),
            'answers' => $request->input('answers', $question->answers),
        ]);
        return response()->json("success");
    }

    function viewquestions(Request $request){
        $db = Questions::where("id",$request->id)->first();
        return response()->json($db);
    }
    function loadfaqs(Request $request){
        $db = FAQS::where("id",$request->id)->first();
        return response()->json($db);
    }
    public function questionsupdate(Request $request)
    {
        $question = Questions::find($request->question_id);
        $question->update([
            'user_id' => $request->input('user_id', $question->user_id),
            'questions' => $request->input('questions', $question->questions),
            'answer' => $request->input('answer', $question->answer),
        ]);

        return response()->json("success");
    }
    function questionaction()
    {
        $questionsQuery = Questions::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if ($start_date && $end_date) {

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $questionsQuery->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
        }
        $questions = $questionsQuery->select('*');
        return datatables()->of($questions)->make(true);

    }
    function faqsaction()
    {
        $questionsQuery = FAQS::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if ($start_date && $end_date) {

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $questionsQuery->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
        }
        $questions = $questionsQuery->select('*');
        return datatables()->of($questions)->make(true);

    }


    public function questiondestroy(User $user, Request $request)
    {
        $db = FAQS::where("id",$request->id)->delete();
        return response()->json('success');
//        if($db){
//            echo "success";
//        }else{
//            echo "error";
//        }
//        return redirect()->route('admins.index')->withSuccess('User has been Deleted');
    }
}
