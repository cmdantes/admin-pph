<?php

namespace App\Http\Controllers;

use App\Client;
use App\Form0605;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ClientsController extends Controller
{
    public function index(){
        return view('users.clients');
    }

    public function actionlist(){
        {
            $client = Client::query();

            $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
            $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

            if($start_date && $end_date){

                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));
                $client->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
            }
            $clients = $client->where('user_id', Auth::user()->id)->get();
            return datatables()->of($clients)->make(true);
        }

    }

    public function update(Request $request)
    {
        $client = Client::find($request->update_id);
        $client->update([
            'user_id' =>  $request->input('user_id', $client->user_id),
            'clientNo' =>  $request->input('clientNo', $client->clientNo),
            'clientName' => $request->input('clientName', $client->clientName),
            'tin' => $request->input('tin', $client->tin),
            'email' => $request->input('email', $client->email),
            'address' => $request->input('address', $client->address),
            'phone' => $request->input('phone', $client->phone),
        ]);
        $client->save();
        return $client;
    }

    public function destroy( Request $request)
    {
        $client = Client::where('user_id', $request->user_id)->orWhere("clientNo", $request->clientNo)->delete();
        if(isset($client)) {
            return 'Success';
        }
    }

    public function create(Request $data)
    {
        Client::create([
            'user_id' => Auth::user()->id,
            'clientNo' => $data['clientNo'],
            'clientName' =>$data['clientName'],
            'tin' => $data['tin'],
            'email' => $data['email'],
            'address' => $data['address'],
            'phone' => $data['phone'],
        ]);
        $client = Client::all()->where('user_id', $data->user_id)->first();
        return response()->json($client);

//        return redirect()->route('clients.index')->withSuccess('User has been Deleted');

    }

    public function showcreate(Client $client){
        return view('users.createClient',compact(['client']));
    }

    function getClients(Request $request){

        $client = Client::where('user_id', $request->user_id)->get();
        return response()->json($client);
    }
    function getClientNo(Request $request){
        $c = Client::where('user_id', $request->user_id)->get(['clientNo']);
        return (string) $c;
    }

}
