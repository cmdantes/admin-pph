<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Tax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaxController extends Controller
{
    public function index(){
        return view('users.tax');
    }

    public function createTax(Request $data){
        Tax::create([
            'user_id' => Auth::user()->id,
            'transactionNo' => $data['transactionNo'],
            'orNumber' =>$data['orNumber'],
            'clientName' => $data['clientName'],
            'service' => $data['service'],
            'amount' => $data['amount'],
            'outOfPocketExpenses' => $data['outOfPocketExpenses'],
            'total' => $data['total'],
            'quarter' => $data['quarter'],
            'date' => $data['date'],
        ]);
        $tx = Tax::all()->where('user_id', $data->user_id)->first();
        return response()->json($tx);
    }

    function actionlist(){
        $tax = Tax::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $tax->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
        }
        $taxs = $tax->where('user_id', Auth::user()->id)->get();
        return datatables()->of($taxs)->make(true);
    }

    public function updateTax(Request $request){
        $tx = Tax::find($request->update_id);
        $tx->update([
            'user_id' =>  $request->input('user_id', $tx->user_id),
            'transactionNo' => $request->input('transactionNo', $tx->transactionNo),
            'orNumber' => $request->input('orNumber', $tx->orNumber),
            'clientName' => $request->input('clientName', $tx->clientName),
            'service' => $request->input('service', $tx->service),
            'amount' => $request->input('amount', $tx->amount),
            'outOfPocketExpenses' => $request->input('outOfPocketExpenses', $tx->outOfPocketExpenses),
            'total' => $request->input('total', $tx->total),
            'date' => $request->input('date', $tx->date),
        ]);
        $tx->save();
        return $tx;
    }

    public function getTax(Request $request){
        $tx = Tax::where('user_id', $request->user_id)->get();
        return response()->json($tx);
    }

    public function getTransactionNo(Request $request){
        $t = Tax::where('user_id', $request->user_id)->get(['transactionNo']);
        return (string) $t;
    }

    public function deleteTax(Request $request){
        $serv = Tax::where('id', $request->id)->where('user_id', $request->user_id)->first()->delete();
        if(isset($serv)) {
            return 'Success';
        }
    }

    public function showCreateTax(Tax $t){
        return view('users.createTax',compact(['t']));
    }
}
