<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Requests\TicketUpdateRequest;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->paginate(10);
        // dd($tickets);
        return view('admins.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketUpdateRequest $request)
    {
        # dd($request);
       $user =  User::create([
            'name' =>$request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);
        return $this->jsonResponse(User::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
       // $statuses = Status::get();
        return view('users.show', compact(['user']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $user->update([
            'name' =>  $request->input('name', $user->name),
            'email' => $request->input('email', $user->email),
            'password' => $request->input('password', $user->password),
        ]);
        $user->status = request('status');

        $user->save();

        return redirect()->route('users.index')->withSuccess('Ticket has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
/*    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return redirect()->route('tickets.index');

    }

    public function delete(Ticket $ticket){

        return view('tickets.delete', compact('ticket'));
    }*/

    public function updateUserInfo()
    {
        return view('users.userInfo');
    }

    public function registerUserInfo(Request $data)
    {
        UserInfo::create([
            'user_id' => Auth::user()->id,
            'firstname' => $data['firstname'],
            'midname' => $data['midname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'address' => $data['address'],
            'zipcode' => $data['zipcode'],
            'phone' => $data['phone'],
            'citizenship' => $data['citizenship'],
            'rdo' => $data['rdo'],
            'lob' => $data['lob'],
            'atc' => $data['atc'],
            'tin' => $data['tin'],
        ]);
        $userInfo = UserInfo::all()->where('user_id', $data->user_id)->first();
        return response()->json($userInfo);
    }

}
