<?php

namespace App\Http\Controllers;

use App\XML;
use Illuminate\Http\Request;

class XMLController extends Controller
{
    function viewXml(Request $request){
        $db = XML::where("id",$request->id)->first();
//        dd($db);
        return $db;
    }

    public function createXML(Request $req){
        $data = array();
        $result  =  XML::where("id",$req->id)->first();
        if($result != "" && $result->file != ""){
            $xmlData = $result->file;
            $cacheFile = $result->file_name;
            ob_start();
            ob_end_clean();
            $results = file_put_contents($cacheFile,$xmlData);
            if($results){
                $data["data"] = $xmlData;
                $data["file_name"] = $result->file_name;
                return json_encode($data, JSON_PRETTY_PRINT);
            }
        }else{
            return response()->json("empty");
        }

    }

    function xml(Request $request, $id){
//        $db = XML::where('user_id',$request->user_id);
//        $id_ = XML::whereUserId(3)->first();
        $id_ = XML::whereUserId($id)->first();
        if(isset($id_)) {
//        return redirect()->route('admin.xml', ['id' => $id_]);
            return view('admins.userxml', compact('id_'));
        }
        else{
            return view('admins.userxml2');
        }

    }
    function files(Request $request){

        $id_ = XML::all();
        return view('admins.files', compact(['id_']));

    }

    function files_load(Request $request){
        $user = XML::query();
        //            $user = DB::table('tbl_users')->join('tbl_user_info','tbl_user_info.user_id', "=",'tbl_users.id');

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if ($start_date && $end_date) {

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $user->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");
        }
        $users = $user->where('status','=','filed');
        return datatables()->of($users)->make(true);
    }

    function xml_load(Request $request, $id){
        $user = XML::query();
        //            $user = DB::table('tbl_users')->join('tbl_user_info','tbl_user_info.user_id', "=",'tbl_users.id');

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        if ($start_date && $end_date) {

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));
            $user->whereRaw("Date(created_at) >= '" . $start_date . "' AND Date(created_at) <= '" . $end_date . "'");

//                ->whereRaw("tbl_users.created_at >= '" . $start_date . "' AND tbl_users.created_at <= '" . $end_date . "'");
        }
//        $users = $user->select('*');
//        $users = $user->find($id);
        $users = $user->where('user_id', $request->id);
        return datatables()->of($users)->make(true);
    }

}
