<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        //admin
        '/admin/register',
        '/admin/dash',
        '/admin/login',
        '/admin/xml',
        '/admin/xml/view/file',
        '/admin/xml/view/',
        '/admin/faqs/update',
        '/admin/faqs/loadQuestions',
        '/admin/faqs/loadFaqs',
        '/admin/faqs/viewQuestions',
        '/admin/questions/destroy',
        '/admin/faqs/update1',
        '/admin/delete',
        '/admin/faqs/insertFaqs',
        '/admin/xml/createXML',
        '/admin/status/update',
        '/admin/status/update1',
        //end admin

        //user
        '/user/login',
        '/user/newUser',
        '/user/updateUserInfo',
        '/user/registerUserInfo',
        '/user/forgotPass',
        '/user/resetPass',
        '/user/password/reset',
        //end user

        //form0605
        '/form0605/getData',
        '/form0605/fileForm0605',
        '/form0605/createForm0605',
        '/form0605/createReport',
        //end form0605

        //billings
        '/billings/getBillingsNo',
        '/billings/getTaxDue',
        '/billings/createBillings',
        '/billings/fileBillings',
        //end billings

        //form 1700a
        '/form1701a/createForm1701a',
        '/form1701a/getData',
        '/form1701a/fileForm1701a',
        '/form1701a/saveNonOperatingIncome',
        '/form1701a/getAllOperating',
        '/form1701a/deleteForm1701a',
        //end form1701a

        //clients
        '/clients/getClientNo',
        '/clients/getClients',
        '/clients/createClients',
        '/clients/updateClients',
        '/clients/deleteClients',
        //end clients

        //services
        '/services/getServices',
        '/services/createServices',
        '/services/updateServices',
        '/services/getServiceNo',
        '/services/deleteServices',
        //end services

        //tax
        '/tax/createTax',
        '/tax/getTax',
        '/tax/getTransactionNo',
        //end tax

        //questions
        '/questions/registerNewQuestion',
        '/questions/getAnswers',
        '/questions/getLikeQuestion',
        '/questions/likeQuestion',
        '/questions/getPopularQuestions',
        //end questions
    ];

    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;
}
