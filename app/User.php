<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tbl_users';

    protected $fillable = [
         'username', 'email', 'password', 'status', 'phone', 'code', 'userType', 'verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
/*    protected $attributes = array([
        'status' => 'active',
        'code' => 'asd',
        'userType' => 'Professional',
        'verified' => 'no'
    ]);*/

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function questions()
    {
        return $this->hasMany(Questions::class );
    }
    public function userinfo()
    {
        return $this->hasMany(UserInfo::class, 'user_id', 'id');
    }

    public function clients()
    {
        return $this->hasOne(Client::class,'user_id');
    }

    public function form0605()
    {
        return $this->hasOne(Form0605::class );
    }

    public function form1701a()
    {
        return $this->hasOne(Form1701a::class );
    }

    public function faqs()
    {
        return $this->hasOne(FAQS::class );
    }

    public function userfile(){
        return $this->hasOne(UserFile::class);
    }

    public function nonoperating(){
        return $this->hasOne(NonOperating::class);
    }

    public function likes(){
        return $this->hasOne(Likes::class);
    }

    public function dashboard(){
        return $this->hasOne(Dashboard::class);
    }
}
