<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{

    protected $table = 'tbl_user_questions';

    protected $fillable = [
        'user_id','questions','answer'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function form0605(){
        $this->hasOne(Form0605::class);
    }

    public function form1701a(){
        $this->hasOne(Form1701a::class);
    }
}
