<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'tbl_service';

    protected $fillable = ['user_id','serviceNo',
        'serviceName','particulars','amount','active'];

    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }
}
