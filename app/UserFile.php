<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model
{
    protected $fillable = ['user_id','billing_id','fileName','fileLink','fileStatus'];

    protected $table = 'tbl_user_file';

    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }

    public function billing(){
        $this->belongsTo(Billing::class, 'billing_id');
    }
}
