<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'tbl_client';

    protected $fillable = [
        'user_id','clientNo','clientName','tin','email','address','phone'
        ];
    
    public function user(){
        $this->belongsTo(User::class);
    }

}
