<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQS extends Model
{
    protected $table = 'tbl_questions';

    protected $fillable = [
        'title','questions_no','questions','answers','notes','views','likes'
        ];

    public function likes(){
        $this->hasOne(Likes::class);
    }


}
