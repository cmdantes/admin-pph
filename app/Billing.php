<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'tbl_billing';

    protected $fillable = [
        'user_id','year','quarter','amended',
        'returnPeriod','billingNo','salesRevenueForThisQuarter', 'nonOperatingIncome',
        'nonOperatingIncomeDesc','totalIncomeOfTheQuarter','cumulativeTaxableIncome', 'allowableReductionFromGross',
        'taxableIncomeToDate','taxDue', 'priorYearExcessCredits','taxPaymentForThePreviousQuarter',
        'creditableTaxWithheldForThePreviousQuarter','creditableTaxWithheldPerBIRFormNo2307', 'taxPaidInReturnPreviouslyFiledIfThisIsAnAmendedReturn','status'
    ];

    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }

    public function userfile(){
        $this->hasOne(UserFile::class);
    }
}
