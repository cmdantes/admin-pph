<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{
    protected $table = 'tbl_likes';

    protected $fillable = ['user_id','question_id','likes'];

    public function question(){
        $this->belongsTo(FAQS::class, 'question_id');
    }

    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }
}
