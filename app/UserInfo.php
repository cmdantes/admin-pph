<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table= 'tbl_user_info';

    protected $fillable = [
        'user_id','firstname','midname','lastname','suffix','email',
        'address','zipcode','phone','citizenship','userType','rdo','atc','tin','lob'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
