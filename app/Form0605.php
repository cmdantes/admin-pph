<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form0605 extends Model
{
    protected $table = 'tbl_form_0605';

    protected $fillable = [
      'user_id','period','yearEnded','quarter','dueDate','returnPeriod','atc','taxType'
        ,'classification','mannerPayment','others','typePayment','installmentNo','voluntaryPayment'
        ,'perAudit','investigatingOfficer','taxDeposit','status','question_id'
    ];

    public function users(){
        $this->belongsTo(User::class, 'user_id');
    }

    public function questions(){
        $this->belongsTo(Questions::class, 'question_id');
    }

}
