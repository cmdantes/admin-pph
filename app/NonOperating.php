<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonOperating extends Model
{
    protected $fillable = [
        'user_id','billingNo','description0','description1','description2',
        'description3','description4','description5','description6','description7',
        'description8','description9','description10','description11','description12',
        'description13','description14','description15','description16','description17',
        'description18','description19','description20','amount0','amount1','amount2',
        'amount3','amount4','amount5','amount6','amount7','amount8','amount9','amount10',
        'amount11','amount12','amount13','amount14','amount15',
        'amount16','amount17','amount18','amount19','amount20'
    ];
    protected $table = 'tbl_non_operating';

    public function user(){
        $this->belongsTo(User::class,'user_id');
    }

}
