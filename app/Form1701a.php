<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form1701a extends Model
{
    protected $table = 'tbl_form_1701a';

    protected $fillable = [
        'user_id','billingNo','quarter','year','amended','returnPeriod','salesRevenue'
        ,'salesReturn','netSales','nonOperatingDesc1','nonOperatingAmount1','nonOperatingDesc2'
        ,'nonOperatingAmount2','totalNonOperatingIncome','totalTaxableIncome','allowableReductionFromGross'
        ,'taxableIncomeToDate','taxDue','page2PriorYear','page2TaxPayment','page2Tax3Quarter'
        ,'page2Creditable','page2TaxPaidReturn','page2OtherTaxCredits','page2TotalTaxCredits'
        ,'page2TotalNetTaxable','status','question_id'
    ];

    public function users(){
        $this->belongsTo(User::class, 'user_id');
    }

    public function questions(){
        $this->belongsTo(Questions::class, 'question_id');
    }

//    public function form1701a(){
//        $this->belongsTo(__CLASS__, 'form_id');
//    }
   
}
