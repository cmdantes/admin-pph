<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    //

    protected $table= 'tbl_taxes';

    protected $fillable = [
        'user_id','transactionNo','date'
        ,'orNumber','clientName','service',
        'amount','outOfPocketExpenses','status','quarter'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
