<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XML extends Model
{
    protected $table = 'tbl_xml';

    protected $fillable = ['user_id','form_id', 'form','file_name','file', 'returnPeriod','status'];


    public function user(){
        $this->belongsTo(User::class, 'user_id');
    }


    public function form(){
        $this->belongsTo(Form0605::class, 'form_id');
    }
}
