<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {
    Route::post('/register','Auth\AdminRegisterController@register')->name('admin.register.submit');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/update','AdminController@update')->name('admins.update');

    Route::post('/faqs/update', 'QuestionsController@questionsupdate')->name('question.update');
    Route::post('/faqs/update1', 'QuestionsController@faqsupdate')->name('questionsupdate');
    Route::post('/faqs/loadFaqs', 'QuestionsController@loadfaqs')->name('questionsupdate');
    Route::post('/faqs/viewQuestions', 'QuestionsController@viewquestions')->name('questionsupdate');
    Route::post('/questions/destroy', 'QuestionsController@questiondestroy')->name('faqsdestroy');
    Route::post('/faqs/insertFaqs', 'QuestionsController@insertFaqs')->name('admins.questions');

    Route::post('/status/update1', 'AdminController@statUpdate')->name('questionsupdate');
    Route::post('/delete','AdminController@destroy')->name('admins.destroy');
    Route::post('/status/update', 'AdminController@updateStatus')->name('updateStatus');

    Route::post('/xml/view/', 'XMLController@viewXml')->name('viewXml');
    Route::post('/xml/createXML/', 'XMLController@createXML')->name('createXML');

    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/dash/action', 'AdminController@actionss')->name('actionlist');
    Route::get('/dash/action2', 'AdminController@anot')->name('anot');
    Route::get('/register','Auth\AdminRegisterController@showRegisterForm')->name('admin.register');

    Route::get('/xml/{id}', 'XMLController@xml')->name('admin.xml');
    Route::get('/xml/action/{id}', 'XMLController@xml_load')->name('xml.load');
    Route::get('/xml/actionss/{id}', 'XMLController@files_load')->name('files_load');

    Route::get('/questions/action', 'QuestionsController@questionaction')->name('admins.questionsaction');
    Route::get('/questions/faqs', 'QuestionsController@faqsaction')->name('faqsaction');
});
Route::get('/admin-pph', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::group(['middleware' => 'preventbackhistory'],function()
{
    Auth::routes();
    Route::get('/faqs', 'AdminController@questions')->name('admins.questions');
    Route::get('/users', 'AdminController@index')->name('admins.index');
    Route::get('/files', 'XMLController@files')->name('admins.files');
    Route::get('/index', 'AdminController@dashes')->name('admins.dash');

});

