
<!doctype html>
<html lang="en">
@include('layouts.partials.head')

<body>
{{--@include('layouts.partials._sidebar')--}}
@include('layouts.partials.navigation')

<div class="container-fluid">
    @yield('content')
</div>
@include('layouts.partials.footer')
@include('layouts.partials.scripts')

</body>

</html>
