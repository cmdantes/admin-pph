<div class="form-group">
    <label for="status"></label>
    <select class="form-control" id="status" name="status" value="{{ !empty($users) ? $users->created_at : ""}}">
        @foreach($users as $stat)
            <option value="{{$stat->created_at}}" {{ !empty($stat) && $stat->created_at}}>{{$status->created_at}}</option>
        @endforeach
    </select>
</div>
