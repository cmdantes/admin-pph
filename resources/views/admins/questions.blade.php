@extends('layouts.main')

@section('content')
    <div class="main">
        <!-- MAIN CONTENT -->
        <div class="main-content">
            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="panel panel-headline">
                        <div class="panel-heading">
                            <h3 class="panel-title">FAQ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="custom-tabs-line tabs-line-bottom left-aligned" style="font-style: normal">
                                        <ul class="nav" role="tablist">
                                            <li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Received Questions</a></li>
                                            <li><a href="#tab-bottom-left2" role="tab" data-toggle="tab">Published Questions <!--<span class="badge">7</span>--></a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <h5>Start Date <span class="text-danger"></span></h5>
                                                    <div class="controls">
                                                        <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date" value="{{date("Y-m-01")}}"> <div class="help-block"></div></div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <h5>End Date <span class="text-danger"></span></h5>
                                                    <div class="controls">
                                                        <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date" value="{{date("Y-m-d")}}"> <div class="help-block"></div></div>
                                                </div>
                                                <div class="text-left" style="margin-left: 15px;">
                                                    <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Filter</button>
                                                </div>
                                            </div>
                                        <div class="tab-pane fade in active" id="tab-bottom-left1">
                                            <form class="form-horizontal publish-questions" id="publish-questions">
                                                <div class="table-responsive">
                                                <table id="zero_config2" class="table table-bordered" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                       {{-- <th><label class="control-inline fancy-checkbox">
                                                                <input type="checkbox" class="chk_boxes hide-checkbox" disabled><span></span>
                                                            </label></th>--}}
                                                        <th></th>
                                                        <th>Questions</th>
                                                        <th>Date Created</th>
                                                        <th>Options</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                                </div>
                                                <div class="text-center"><button type="button" class="btn btn-success publish">Publish</button></div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="tab-bottom-left2">
                                            <div class="table-responsive">
                                                <table id="zero_config" class="table  table-bordered" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Questions</th>
                                                        <th>Options</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Delete Confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('faqsdestroy')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <p class="text-center">ArE YoU SuRe YoU wAnT tO DeLeTe?</p>
                        <input type="hidden" id="category_id" name="category_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>--}}
    <div class="modal fade" id="view-questions">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal save-questions" id="save-questions">
                    <div class="modal-header" style="background-color: #00aff0;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-center" style="color: white">ANSWER</h4>
                    </div>
                    <div class="modal-body">
                        <ul class="list-unstyled todo-list">
                            <li>
                                <p>
                                    <span class="title">User Question *</span>
                                    <br><br>
                                    <textarea name="question_id" class="question_id" id="question_id" hidden></textarea>
                                    <textarea class="form-control" placeholder="User Question" name="question" class="question" id="question" rows="4" required></textarea>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <span class="title">Your Answer *</span>
                                    <br><br>
                                    <textarea class="form-control" placeholder="Place Your Answer" name="answer" class="answer" id="answer"  rows="4" required></textarea>
                                </p>
                            </li>
                        </ul>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="saver">Save</button>

                    </div>


                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="view-questionnaire">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal save-questionnaire" id="save-questionnaire">
                    <div class="modal-header" style="background-color: #00aff0;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-center" style="color: white">ANSWER</h4>
                    </div>
                    <div class="modal-body">
                        <ul class="list-unstyled todo-list">
                            <li>
                                <p>
                                    <span class="title">User Question *</span>
                                    <br><br>
                                    <textarea name="questionnaire_id" class="questionnaire_id" id="questionnaire_id" hidden></textarea>
                                    <textarea class="form-control" placeholder="User Question" name="questionnaire" class="questionnaire" id="questionnaire" rows="4" required></textarea>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <span class="title">Your Answer *</span>
                                    <br><br>
                                    <textarea class="form-control" placeholder="Place Your Answer" name="answers" class="answers" id="answers"  rows="4" required></textarea>
                                </p>
                            </li>
                        </ul>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >Update</button>

                    </div>


                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-delete-user">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">CONFIRM</h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <div class="msg text-center">
                            Are you sure you want to delete this?
                        </div>
                    </h4>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger confirm-btn" data-dismiss="modal">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-delete-questions">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">CONFIRM</h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <div class="msg text-center">
                            Are you sure you want to delete this?
                        </div>
                    </h4>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger confirm-btn2" data-dismiss="modal">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="system-msg">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">CONFIRM</h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <div class="msg text-center message-body">
                        </div>
                    </h4>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <button type="button" class="btn btn-default " data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-publish-message">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">CONFIRM</h4>
                </div>
                <div class="modal-body">
                    <br>
                    <div class="form-group">
                        <p class="text-center" >Are you sure you want to publish this <br> Q & A ?</p>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="form-group pull-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-primary confirm-publish">Yes</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        $(document).ready( function () {
            var table = $('#zero_config2').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url:"{{ route('admins.questionsaction') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },

                'select': {
                    'style': 'multi'
                },
                order: [[1, 'asc']],
                columns: [
                    {
                        'data': 'id',
                        'name':'id',
                        'checkboxes': {
                            'selectRow': true
                        }
                    },
                    { data: 'questions', name: 'questions' },
                    { data: 'created_at', name: 'created_at' },
                    {
                        searchable: false,
                        data : function (data) {
                            return '<a class="btn btn-success btn-xs"  id="update-id" data-user_id="'+data.user_id+'" data-id="'+data.id+'"   data-questions="'+data.questions+'" data-answer="'+data.answer+'"  data-toggle="modal" data-target="#view-questions" onclick="viewQuestions(this)"> VIEW ANSWER </a>'
                        }
                    },

                ],
            });

            $('.publish').click(function (e) {
                let selectedRows = table.column(0).checkboxes.selected();
                let selectedIds =[];

                $.each(selectedRows, function (index, rowsId) {
                    selectedIds.push(rowsId);
                    console.log(selectedIds);
                });

                // alert(selectedIds)
                e.preventDefault();  //prevent form from submitting
                publish(selectedIds);

            });
            function publish(id) {
                if(id ==""){
                    alert("Please select Q and A to publish");
                }else{
                    $('#confirm-publish-message').modal('toggle');
                    $('.confirm-publish').on('click', function(){
                        $.post('/admin/faqs/insertFaqs', {
                            id: id
                        }, function(data) {
                            console.log(data);
                            if(data === "success") {
                                alert("You have successfully published this Q and A.");
                                location.reload();
                            }else if(data === "answer"){
                                alert("Please select Q and A to publish");
                            }else{
                                alert("Error Publishing this Q and A.");
                            }
                        })
                            .fail(function(xhr, textStatus, errorThrown){
                                alert(xhr.responseText);
                            });
                    })
                }
            }

        });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready( function () {
            $('#zero_config').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url:"{{ route('faqsaction') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columns: [
                    { data: 'questions', name: 'questions' },
                    {
                        searchable: false,
                        data : function (data) {
                            return '<button class="btn btn-primary btn-xs"  id="update-id" data-user_id="'+data.user_id+'" data-id="'+data.id+'"   data-questions="'+data.questions+'" data-answer="'+data.answers+'" data-target="#view-questionnaire"  onclick="loadFaqs(this)"><i class="fa fa-pencil"></i></button>'+
                                // '<a href="/admin/delete/'+data+'">' + "Delete" + '</a> '
                                '<button class="btn btn-danger btn-xs"  onclick="destroy(this)" data-id="' + data.id + '" ><i class="fa fa-trash-o" ></i></button> '
                        }
                    },
                ]
            });
        });

        $('#btnFiterSubmitSearch').click(function(){
            $('#zero_config2').DataTable().draw(true);
            $('#zero_config').DataTable().draw(true);
        });
        $(window).on('load', function() {
        // window.onload = function() {
            var check = [];
            $(".chk_boxes1").change(function(){
                check = [];
                var id = $(this).attr('id');
                $(':checkbox[name="checked[]"]:checked').each (function () {
                    check.push(this.id);
                    console.log(check);
                });
            });

            $('.publish-questions').on('submit',function(e) {
                e.preventDefault();  //prevent form from submitting
                publish(check);
            });
        });
        function publish(id) {
            if(id ==""){
                alert("Please select Q and A to publish");
            }else{
                $('#confirm-publish-message').modal('toggle');
                $('.confirm-publish').on('click', function(){
                    $.post('/admin/faqs/insertFaqs', {
                        id: id
                    }, function(data) {
                        console.log(data);
                        if(data === "success") {
                            alert("You have successfully published this Q and A.");
                            location.reload();
                        }else if(data === "answer"){
                            alert("Please select Q and A to publish");
                        }else{
                            alert("Error Publishing this Q and A.");
                        }
                    })
                        .fail(function(xhr, textStatus, errorThrown){
                            alert(xhr.responseText);
                        });
                })
            }
        }
        $(function(){
            $('.chk_boxes').click(function() {
                $('.chk_boxes1').prop('checked', this.checked);
            });
        });

        function viewQuestions(identifier) {
            var id=$(identifier).data('id');
            $.post('/admin/faqs/viewQuestions', {
                id: id
            }, function(data) {
                json = jQuery.parseJSON(JSON.stringify(data));
                console.log(json);
                $('#question').html(json.questions);
                $('#answer').html(json.answer);
                $('#question_id').html(json.id);
                var saver =  document.getElementById("saver");
                json.answer=='' ? saver.innerHTML ='Save' :saver.innerHTML ='Update';
                $('#view-questions').modal('toggle');
            });

        }

        function loadFaqs(identifier)
        {
            var id=$(identifier).data('id');
            $.post('/admin/faqs/loadFaqs', {
                id: id
            }, function(data) {
                json = jQuery.parseJSON(JSON.stringify(data));
                console.log(json);
                $('#questionnaire').html(json.questions);
                $('#answers').html(json.answers);
                $('#questionnaire_id').html(json.id);
                $('#view-questionnaire').modal('toggle');
            });
        }

        $('.save-questionnaire').on('submit',function(event){
            event.preventDefault();
            var faqs = confirm("Are you sure you want to update the question?");
            if(faqs == false){
                return false;
            }else{
                saver();
            }
        });

        function saver(identifier){
            var inputs = $('.save-questionnaire').serialize();
            var id=$(identifier).data('id');
            $.post('/admin/faqs/update1', inputs,function(data) {
                    console.log(data);
                    if (data === "success") {
                        alert("You have successfully updated the question.");
                        location.reload();
                    } else {
                        alert("Error Updating Receive Questions");
                    }
                });
            }


        $('.save-questions').on('submit', function(event){
            event.preventDefault();

            var faqs = confirm("Are you sure you want to save this answer?");

            if(faqs == false){
                return false;
            }else{
                saved();
            }

        });

        function saved(){
            var inputs = $('.save-questions').serialize();
            $.post('/admin/faqs/update', inputs,function(data) {
                    console.log(data);
                    if(data === "success"){
                        alert("You have successfully answered this question.");
                        location.reload();

                    }else{
                        alert("Error Updating Questions");

                    }
                })
                .fail(function(xhr, textStatus, errorThrown){
                    alert(xhr.responseText);
                });

        }

        function destroy(identifier)
        {
            var id=$(identifier).data('id');
            $('#confirm-delete-questions').modal('toggle');
            $('.confirm-btn2').on('click', function(){
                $.post('/admin/questions/destroy', {
                    id: id
                }, function(data) {
                    if(data === "success"){
                        timeout = window.setTimeout(function() {
                            timeout = null;
                            toastr.success("You have successfully deleted this Q and A.");
                        }, 1000);
                        timeout = window.setTimeout(function() {
                            timeout = null;
                            location.reload();
                        }, 3000);

                    }else{
                        timeout = window.setTimeout(function() {
                            timeout = null;
                            toastr.error("Error Deleting Published Questions");
                        }, 1000);
                    }
                })
            .fail(function(xhr, textStatus, errorThrown){
                    alert(xhr.responseText);
                });
            })
        }

        // Handle form submission event
        // $('#save-questionnaire').on('submit', function(e) {
        //     var form = this;
        //
        //     var rows_selected = table.column(0).checkboxes.selected();
        //
        //     // Iterate over all selected checkboxes
        //     $.each(rows_selected, function (index, rowId) {
        //         // Create a hidden element
        //         $(form).append(
        //             $('<input>')
        //                 .attr('type', 'hidden')
        //                 .attr('name', 'id[]')
        //                 .val(rowId)
        //         );
        //     });
        // })
    </script>
@endsection



