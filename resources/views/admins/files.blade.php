@extends('layouts.main')

@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Users XML Files</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <h5>Start Date <span class="text-danger"></span></h5>
                                        <div class="controls">
                                            <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date" value="<?php echo date("Y-m-01"); ?>"> <div class="help-block"></div></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h5>End Date <span class="text-danger"></span></h5>
                                        <div class="controls">
                                            <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date" value="<?php echo date("Y-m-d"); ?>"> <div class="help-block"></div></div>
                                    </div>
                                    <div class="text-left" style="margin-left: 15px;">
                                        <button type="text" id="btnFiterSubmitSearch" class="btn btn-primary">Filter</button>
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="zero_config" style="width:100%">
                                            @csrf
                                            <thead>
                                            <tr>
                                                <th>Form</th>
                                                <th>Filename</th>
                                                <th>Status</th>
                                                <th>Date Created</th>
                                                <th>XML</th>
                                            </tr>
                                            </thead>
                                        </table>
                                        {{--  <form action="{{route('form0605.createReport')}}" enctype="multipart/form-data" method="post">
                                              {{ csrf_field() }}
                                              <br><br>
                                              Attach Files <br>
                                              <input type="file" name="file">
                                              <br><br>
                                              <input type="submit" class="btn btn-info" value="Upload">
                                          </form>--}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="view-xml">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">XML</h4>
                </div>
                <div class="modal-body">
                    <div id="xml"></div>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        // Datatable
        $(document).ready( function () {
            $('#zero_config').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },

                responsive: true,
                bPaginate : false,
                bLengthChange : false,
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                ajax: {
                    url:"{{ route('files_load', ['id']) }}",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columns: [
                    { data: 'form', name: 'form' },
                    { data: 'file_name', name: 'file_name' },
                    {data: 'status', name: 'status',
                        // options: [
                        //     { label: "Filed", value: "Filed" },
                        //     { label: "Pending", value: "Pending" },
                        //     { label: "Saved",  value: "Saved" }
                        // ],
                        render: function (data, type, row)  {
                            if(row.status === 'Filed' || row.status === 'filed') {
                                return '<span class="label label-success">Filed<span>'   }
                            else if(row.status === 'Saved' || row.status === 'saved'){
                                return '<span class="label label-info">Saved<span>'  }
                            else {
                                return '<span class="label label-warning">Pending<span>'  }
                        }
                    },
                    { data: 'created_at', name: 'created_at' },
                    {
                        searchable: false,
                        data : function (data) {
                            return '<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-user_id="'+data.user_id+'" data-id="'+data.id+'" data-file="'+data.file+'" data-filename="'+data.file_name+'" data-target="#view-xml" onclick="viewXML(this)"><i class="fa fa-file-excel-o" ></i></button>'+
                                // '<a href="/admin/delete/'+data+'">' + "Delete" + '</a> '
                                '<button type="button" class="btn btn-primary btn-xs" onclick="createXML(this)" data-user_id="'+data.user_id+'" data-filename="'+data.file_name+'" data-id="'+data.id+'"><i class="fa fa-download" ></i></button> '
                        }
                    },

                ],

            });
        });
        $('#btnFiterSubmitSearch').click(function(){
            $('#zero_config').DataTable().draw(true);
        });
        function createXML(identifier){
            var timeout;
            var id=$(identifier).data('id');
            $.ajax({
                url: '/admin/xml/createXML',
                method: 'POST',
                data: { id: id },
                dataType: 'json',
                success : function(data) {
                    if(data){
                        var element = document.createElement('a');
                        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data['data']));
                        element.setAttribute('download',data['file_name']);
                        element.style.display = 'none';
                        document.body.appendChild(element);
                        element.click();
                        document.body.removeChild(element);
                        download(data['data'],data['file_name']);
                    }else {
                        timeout = window.setTimeout(function() {
                            timeout = null;
                            toastr.warning("No xml file created in database!");
                        }, 1000);
                    }
                },
                error: function(xhr,jqXHR, error, errorThrown){
                    console.log(xhr.responseText);
                    timeout = window.setTimeout(function() {
                        timeout = null;
                        toastr.warning("No xml file created in database!");
                    }, 1000);
                }
            });

        }

        function viewXML(identifier){
            var id=$(identifier).data('id');
            $.post('/admin/xml/view',{'id':id})
                .done(function (result) {
                    json = jQuery.parseJSON(JSON.stringify(result));
                    if(json['file'] != ""){
                        var xmlTextNode = document.createTextNode(json['file']);
                        var parentDiv = document.getElementById('xml');
                        parentDiv.appendChild(xmlTextNode);
                    }else{
                        alert("No xml file created in database!.");
                    }

                })
                .fail(function(e){
                    alert('fail');
                });
        }

    </script>
@endsection
