@extends('layouts.main')

@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">Users</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <h5>Start Date <span class="text-danger"></span></h5>
                                        <div class="controls">
                                            <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date" value="<?php echo date("Y-m-01"); ?>"> <div class="help-block"></div></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h5>End Date <span class="text-danger"></span></h5>
                                        <div class="controls">
                                            <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date" value="<?php echo date("Y-m-d"); ?>"> <div class="help-block"></div></div>
                                    </div>
                                    <div class="text-left" style="margin-left: 15px;">
                                        <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Filter</button>
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                    <table class="table table-bordered" id="zero_config" style="width:100%">
                                        @csrf
                                        <thead>
                                        <tr>
                                            <th>Email Address</th>
                                            <th>Registration Date</th>
                                            <th>Action</th>
                                            <th>Option</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="confirm-delete-user">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">CONFIRM</h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <div class="msg text-center">
                            Are you sure you want to delete this?
                        </div>
                    </h4>
                </div>
                <div class="modal-footer ">
                    <div class="text-center">
                        <button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger confirm-btn" data-dismiss="modal">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal View -->
    <div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center"><b>- USER INFORMATION -</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="panel-body no-padding bg-primary text-center" id="noContent">
                    <div class="padding-top-30 padding-bottom-30">
                        <i class="fa  fa-user-secret fa-5x"></i>
                        <h3>No Current Info</h3>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="panel-body" id="withContent">
                        <ul class="list-unstyled activity-list">
                            <li>
                                <p>Name<span class="timestamp name" ></span></p>
                            </li>
                            <li>
                                <p>TIN Number<span class="timestamp tin"></span></p>
                            </li>
                            <li>
                                <p>RDO<span class="timestamp rdo"></span></p>
                            </li>
                            <li>
                                <p>Line of Business<span class="timestamp business"></span></p>
                            </li>
                            <li>
                                <p>Address<span class="timestamp address"></span></p>
                            </li>
                            <li>
                                <p>Zip Code<span class="timestamp zip"></span></p>
                            </li>
                            <li>
                                <p>Telephone<span class="timestamp phone"></span></p>
                            </li>
                            <li>
                                <p>Email<span class="timestamp email"></span></p>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="modal-footer foot">
                    <center>
                        <button  class="btn btn-success submitForm"  id="id" onclick="clicks(this);">VIEW SUBMITTED FORMS</button>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <!--Modal Update -->
    <div class="modal fade" id="view-questionnaire">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal save-questionnaire" id="save-questionnaire">
                    <div class="modal-header" style="background-color: #00aff0;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-center" style="color: white">Update Status</h4>
                    </div>
                    <div class="modal-body">
                        <ul class="list-unstyled todo-list">
                            <li>
                                <p>
                                    <span class="title">Email</span>
                                    <br><br>
                                    <input type="hidden" id="update_id" name="update_id">
                                    <input type="text" id="email" name="email" class="form-control" disabled/>
                                    {{--<textarea name="questionnaire_id" class="questionnaire_id" id="questionnaire_id" hidden></textarea>--}}
                                    {{--<textarea class="form-control" name="questionnaire" id="questionnaire" disabled></textarea>--}}
                                    {{--<input name="questionnaire" id="questionnaire" class="form-control" disabled>--}}
                                    {{--<span class="timestamp questionnaire" ></span>--}}
                                </p>
                            </li>
                            <li>
                                <p>
                                    <span class="title">Status</span>
                                    <br><br>
                                    <select class="form-control" id="status" name="status">
                                        <option value="deactivate">Deactivate</option>
                                        <option value="active">Activate</option>
                                    </select>
                                    {{--<textarea class="form-control" placeholder="Place Your Answer" name="answers" class="answers" id="answers"  rows="4" required></textarea>--}}
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" >Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        // Datatable
        $(document).ready( function () {
            $('#zero_config').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true,
                bPaginate : false,
                bLengthChange : false,
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                ajax: {
                    url:"{{ route('actionlist') }}",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columns: [
                    { data: 'email', name: 'email' },
                    { data: 'created_at', name: 'created_at' },
                    {
                        searchable: false,
                        data : function (data)   {
                            if(data.status !== "active"){
                                return '<button class="show-modal btn btn-success"  id="update"  data-id="'+data.id+'" data-name="'+data.name+'" data-email="'+data.email+'" data-status="'+data.status+'" onclick="statusChange(this)"><i class="fa fa-check-circle"> Activate</i></button>'
                            }else{
                                return '<button class="show-modal btn btn-danger"  id="update"  data-id="'+data.id+'" data-name="'+data.name+'" data-email="'+data.email+'" data-status="'+data.status+'" onclick="statusChange(this)"><i class="fa fa-close"> Deactivate</i></button>'
                            }

                        }
                    },
                    {
                        searchable: false,
                        data : function (data) {
                            return '<button type="button" class="btn btn-primary btn-xs viewData" data-id="' + data.id + '"  data-toggle="modal" data-target="#view" onclick="viewtbl(this)" ><i class="fa fa-eye"></i></button>'+
                                // '<a href="/admin/delete/'+data+'">' + "Delete" + '</a> '
                                '<button class="btn btn-danger btn-xs"  onclick="deleteUser(this)" data-id="' + data.id + '"><i class="fa fa-trash-o" ></i></button> '
                        }
                    },
                ],
            });
        });

        // Search Filter
        $('#btnFiterSubmitSearch').click(function(){
            $('#zero_config').DataTable().draw(true);
        });
        $("#update").click(function() {
            $(this).toggleClass('btn-default btn-danger');

        })
        //Update Modal
        function statusChange(identifier)
        {

            var id=$(identifier).data('id');
            $.post('/admin/status/update', {
                id: id
            }, function(data) {
                json = jQuery.parseJSON(JSON.stringify(data));
                console.log(json);
                $('#email').val(json.email);
                $('#status').val(json.status);
                $('#update_id').val(json.id);
                $('#view-questionnaire').modal('toggle');
            });
        }

        $('.save-questionnaire').on('submit',function(event){
            event.preventDefault();
            var faqs = confirm("Are you sure you want to update?");
            if(faqs == false){
                return false;
            }else{
                saver();
            }
        });

        function saver(identifier){
            var inputs = $('.save-questionnaire').serialize();
            var id=$(identifier).data('id');
            $.post('/admin/status/update1', inputs,function(data) {
                console.log(data);
                if (data === "success") {
                    alert("You have successfully updated the Status.");
                    location.reload();
                } else {
                    alert("Error Updating");
                }
            });
        }

        function clicks(id){
            // alert(result.id)
            location.href= '/admin/xml/'+ id.id;
        }

        function viewtbl(identifier)
        {
            var id=$(identifier).data('id');

            $.get('/admin/dash/action2',{'id':id})
                .done(function (result) {
                    json = jQuery.parseJSON(JSON.stringify(result));
                    console.log(result);
                    if(!json.toString() == ""){
                        $('.name').html(json['firstname'] + " " + result.midname + " " +result.lastname);
                        $('.tin').html(result.tin);
                        $('.business').html(result.lob);
                        $('.rdo').html(result.rdo);
                        $('.address').html(result.address);
                        $('.zip').html(result.zipcode);
                        $('.phone').html(result.phone);
                        $('.email').html(result.email);
                        $('.submitForm').attr('id',result.user_id);
                        $('.submitForm').show();
                        $('#noContent').hide();
                        $('#withContent').show();
                        $('.submitForm').show();
                        $('#view-user').modal('toggle');
                    }else{
                        $('#noContent').show();
                        $('#withContent').hide();
                        $('.submitForm').hide();
                        $('#view-user').modal('toggle');
                    }
                })
                .fail(function(e){
                    alert('Fail')
                });
        }
        // Delete Modal
        function destroy(identifier)
        {
            var id=$(identifier).data('id');
            document.getElementById('category_id').value = id;
        }
        function deleteUser(identifier) {
            var timeout;
            var id=$(identifier).data('id');
            $('#confirm-delete-user').modal('toggle');
            $('.confirm-btn').on('click', function(){
                $.post('/admin/delete', {
                    id: id
                }, function(data) {
                    timeout = window.setTimeout(function() {
                        timeout = null;
                        toastr.success("User was successfully deleted!");
                    }, 1000);
                    timeout = window.setTimeout(function() {
                        timeout = null;
                        location.reload();
                    }, 6000);

                });
            })
        }
    </script>
@endsection
{{--
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Delete Confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admins.destroy')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <p class="text-center">ArE YoU SuRe YoU wAnT tO DeLeTe?</p>
                        <input type="hidden" id="category_id" name="category_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>--}}
